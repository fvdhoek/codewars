"""
    This is mym module.
"""

import heapq
import traceback

def find_shortest_path(grid, start_node, end_node):
    "Find the shortest path from start node to end node."

    #print_grid(grid)

    if len(grid) == 0:
        return []

    try:
        graph = build_graph(grid)
        print graph
        cost, path = shortest_path(graph,
                                   point_to_key(start_node),
                                   point_to_key(end_node))

        print cost, path
        return [grid[key_to_point(key)[0]][key_to_point(key)[1]] for key in path]
    except:
        print traceback.format_exc()
        raise


def shortest_path(graph, start, end):
    "Implementation of Dijkstra's algorithm for shortest paths."

    queue, seen = [(0, start, [])], set()
    while True:
        (cost, v, path) = heapq.heappop(queue)
        if v not in seen:
            path = path + [v]
            seen.add(v)
            if v == end:
                return cost, path
            for (next, c) in graph[v].iteritems():
                heapq.heappush(queue, (cost + c, next, path))



def point_to_key(point):
    "Creates a string key from point tuple."

    if isinstance(point, Node):
        point = (point.position.x, point.position.y)

    return "%d-%d" % point



def key_to_point(key):
    "Creates a point from a key string."

    x, y = key.split('-')
    return (int(x), int(y))



def build_graph(grid):
    "Converts a 2D array into a graph with vertexes."

    def exists_in_grid(grid, point):
        "Validates if point exists in grid."

        r, c = point
        if r < 0 or r > len(grid)-1:
            return False
        if c < 0 or c > len(grid[r])-1:
            return False
        return True


    graph = {}
    height, width = len(grid), len(grid[0])
    for r in range(height):
        for c in range(width):
            key = point_to_key((r, c))
            graph[key] = {}

            # check neighbours
            for nb in [(r-1, c), (r+1, c), (r, c-1), (r, c+1)]:
                if exists_in_grid(grid, nb) and grid[nb[0]][nb[1]].passable:
                    graph[key][point_to_key(nb)] = 1 # always distance of 1

    return graph





import unittest

class NodePosition(object):
    ""

    def __init__(self, x, y):
        self.x = x
        self.y = y


class Node(object):
    """ 
        Represents a node/vertex in the graph
    """

    @property
    def position(self):
        return self._position

    @property
    def passable(self):
        return self._passable

    def __init__(self, position, passable=True):
        self._position = position
        self._passable = passable

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return (self._position.x, self._position.y).__repr__()



class TestChoppa(unittest.TestCase):

    def buildgrid(self, blueprint, height, width):

        grid = range(height)
        grid[0] = range(width)
        r, c = 0, 0
        for v in blueprint:

            if v == '\n':
                r += 1
                c = 0
                grid[r] = range(width)
                continue

            grid[r][c] = Node(NodePosition(r, c), v != '1')
            c += 1

        return grid


    def test_simplegrid(self):
        """
        S0110
        01000
        01010
        00010
        0001E
        """

        gridbp = """S0110
01000
01010
00010
0001E"""
        grid = self.buildgrid(gridbp, 5, 5)
        path = find_shortest_path(grid, grid[0][0], grid[4][4])
        print path


    def test_complexgrid(self):

        gridbp = """0000000000000000000000
S111111111111111110101
0000000000000000010101
1111111011110111010101
0110001000100011110101
0000101010101000000101
0111100010001111101101
0111111111111000100001
0100010001000000111111
0101010101010010000001
0101010101011110111101
00010001000110000011E1
0000000000000000000000
0111111111111111110101
0000000000000000010101
1111111011110111010101
0110001000100011110101
0000101010101000000101
0111100010001111101101
0111111111111000100001
0100010001000000111111
0101010101010010000001
0101010101011110111101
0001000100011000001101"""

        grid = self.buildgrid(gridbp, 24, 22)
        path = find_shortest_path(grid, grid[1][0], grid[11][20])
        print path





if __name__ == '__main__':
    unittest.main()