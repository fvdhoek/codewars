/*! robby v0.0.0 - MIT license */

'use strict';

/**
 * Module dependencies
 */
require('console.table')

/**
 * Basic priority queue implementation. If a better priority queue is wanted/needed,
 * this code works with the implementation in google's closure library (https://code.google.com/p/closure-library/).
 * Use goog.require('goog.structs.PriorityQueue'); and new goog.structs.PriorityQueue()
 */
class PriorityQueue {
  
    /**
     * Create a PriorityQueue instance.
     */
    constructor() {
        this._nodes = [];
    }

    enqueue (priority, key) {
        this._nodes.push({ key: key, priority: priority });
        this.sort();
    };

    dequeue() {
        return this._nodes.shift().key;
    };

    sort() {
        this._nodes.sort(function (a, b) {
            return a.priority - b.priority;
        });
    };

    isEmpty() {
        return !this._nodes.length;
    };
}

/**
 * Implemenation of Dijkstra's shortest path algorithm
 *  see https://github.com/mburst/dijkstras-algorithm/blob/master/dijkstras.js
 */
class Graph {

    /**
     * Create a Graph instance.
     */
    constructor() {
        this.INFINITY = 1/0;
        this.vertices = {};
    }

    /**
     * @param {String} name
     * @param {Object} edges - { <edge>: <distance>,.. }
     */
    addVertex(name, edges) {
        this.vertices[name] = edges;
    };

    /**
     * @param {String} start - Start vertex key
     * @param {String} finish - End vertex key
     * @returns {Array} - An array of vertexes to traverse to get to finish fastest 
     */
    shortestPath(start, finish) {

        var nodes = new PriorityQueue(),
            distances = {},
            previous = {},
            path = [],
            smallest, vertex, neighbor, alt;

        for (vertex in this.vertices) {
            if (vertex === start) {
                distances[vertex] = 0;
                nodes.enqueue(0, vertex);
            }
            else {
                distances[vertex] = this.INFINITY;
                nodes.enqueue(this.INFINITY, vertex);
            }

            previous[vertex] = null;
        }

        while (!nodes.isEmpty()) {
            smallest = nodes.dequeue();

            if (smallest === finish) {
                path = [];

                while (previous[smallest]) {
                    path.push(smallest);
                    smallest = previous[smallest];
                }

                break;
            }

            if (!smallest || distances[smallest] === this.INFINITY) {
                continue;
            }

            for (neighbor in this.vertices[smallest]) {
                alt = distances[smallest] + this.vertices[smallest][neighbor];

                if (alt < distances[neighbor]) {
                    distances[neighbor] = alt;
                    previous[neighbor] = smallest;

                    nodes.enqueue(alt, neighbor);
                }
            }
        }

        return path;
    };
}

/**
 * Implemenation for codewars kata Robby the Robot
 *  see https://www.codewars.com/kata/robby-the-robot
 */
class Robby {

    /**
     * Create a Robby.
     * @param {Object} options - The options value.
     * @param {String} options.field - The field robby needs to traverse.
     * @param {Number} options.power - The power robby has left.
     */
    constructor(options) {
        this.options = options || { field: null, power: null }
        this.field = this.options.field || 'T.S.'
        this.power = this.options.power || 10
        this.fieldSize = Math.round(Math.sqrt(this.field.length));
        this.startPoint = Robby.indexToPoint(this.field.indexOf('S'), this.fieldSize);
        this.finishPoint = Robby.indexToPoint(this.field.indexOf('T'), this.fieldSize);
        this.directions = 
                    {
                        'S': 180,
                        'W': 270,
                        'N': 0,
                        'E': 90
                    }
    }

    /**
     * Returns the list of commands that Robby needs to reach its target.
     */
    getCommands() {
        
        // check if this whole thing is viable.
        let [movex, movey] = Robby.diffBetweenPoints(this.startPoint, this.finishPoint);
        if ((movex + movey) >= this.power) {
            // this will never work because we also need to commands to
            //  turn robby around
            return [];
        }

        this.fieldArray = Robby.constructField(this.fieldSize, this.field);
        let graph = Robby.constructGraph(this.fieldArray, this.fieldSize);
        let path = graph.shortestPath(
            Robby.pointToKey(this.startPoint),
            Robby.pointToKey(this.finishPoint));
        
        path = path.concat([Robby.pointToKey(this.startPoint)]).reverse();
        
        // traverse the path, robby start with heading NORTH.
        var commands = [],
            direction = { current: 'N' };

        for (let i=0; i < path.length; i++) {
            if (!path[i+1]) break;

            commands.push(
                this.toCommands(
                    Robby.keyToPoint(path[i]), 
                    Robby.keyToPoint(path[i+1]),
                    direction).join(''));
        }

        let cmdstr = commands.join("");
        return cmdstr.length > this.power || cmdstr.length == 0 ? [] : [cmdstr];
    }

    

    /**
     * @param {Array} pointFrom - eg. [0,0]
     * @param {Array} pointTo - eg. [0,0]
     * @param {Array} direction - eg. N - North
     * @returns {Array} - An array of string commands
     */
    toCommands(pointFrom, pointTo, direction) {
        // validate if it is a neighbour
        let [movex, movey] = Robby.diffBetweenPoints(pointFrom, pointTo);
            
        let calcDir = (movex, movey) => {
            if (movey > 0) return 'S';
            if (movey < 0) return 'N';
            if (movex > 0) return 'E';
            if (movex < 0) return 'W';
        }

        var commands = []
        let cost = (cd,td) => { 
            if (cd == td) return [0, ''];
            let diff =  this.directions[cd] - this.directions[td];
            let mod_diff = diff % 180.0;
            let dist = mod_diff == 0 ? diff : mod_diff;
            let steps = dist / 90;
            return [Math.abs(steps), diff > 180 ? 'r' : diff < -180 ? 'l' : diff > 0 ? 'l' : 'r'];
        };

        let [c, d] = cost(direction.current, calcDir(movex, movey))
        d.repeat(c).split('').forEach(c => commands.push(c));
        commands.push('f');
        direction.current = calcDir(movex, movey);

        return commands;
    }


    /**
     * @param {Array} pointFrom - eg. [0,0]
     * @param {Array} pointTo - eg. [0,0]
     */
    static diffBetweenPoints(pointFrom, pointTo) {
        // validate if it is a neighbour
        let [pfy, pfx] = pointFrom,
            [pty, ptx] = pointTo;
        let movex = (ptx-pfx),
            movey = (pty-pfy)

        return [movex, movey]
    }

    /**
     * @param {Number} index
     * @param {Number} size
     */
    static indexToPoint(index, size) {
        let row = parseInt(index / size);
        let column = index % size;
        return [row, column];
    }

    /**
     * @param {Number} size
     * @param {String} fieldSize
     */
    static constructField(size, fieldStr) {
        var field = new Array(size);
        for (let i = 0; i < size; i++) {
            field[i] = new Array(size);
            for (let j = 0; j < size; j++) {
                field[i][j] = fieldStr[((i) * size) + j]
            }
        }
        return field;
    }

    /**
     * @param {String} field
     * @param {Array} point
     */
    static existsInField(field, point) {
        var [r, c] = point
        if (r < 0 || r > field.length-1)
            return false;
        if (c < 0 || c > field[r].length-1)
            return false;
        return true
    }

    /**
     * @param {Array} point - point ([1,1]) to convert to key "1-1"
     */
    static pointToKey(point) {
        let [r, c] = point;
        return `${r}-${c}`;
    }

    /**
     * @param {String} key - key to convert to point
     */
    static keyToPoint(key) {
        let [r, c] = key.split('-');
        return [parseInt(r), parseInt(c)];
    }

    /**
     * Converts a 2D array field into a Graph object with vertexes.
     * 
     * @param {Array} field - A 2D array containing all nodes
     * @param {Number} fieldSize - Size of field
     */
    static constructGraph(field, fieldSize) {
        //g.addVertex('A', {B: 7, C: 8});
        let graph = new Graph();
        for (let r = 0; r < fieldSize; r++) {
            for (let c = 0; c < fieldSize; c++) {
                let vertexKey = Robby.pointToKey([r, c])
                var vertexEdges = {};

                // check neighbours
                [
                    { p: [r - 1, c], ec: 0 }, 
                    { p: [r + 1, c], ec: 2 }, 
                    { p: [r, c - 1], ec: 1 }, 
                    { p: [r, c + 1], ec: 1 }
                ].forEach(nb => {
                    if (Robby.existsInField(field, nb.p) && field[nb.p[0]][nb.p[1]] != "#")
                        vertexEdges[Robby.pointToKey(nb.p)] = 1 + nb.ec; // always distance of 1
                });
                // add vertex with edges to graph
                graph.addVertex(vertexKey, vertexEdges);
            }
        }
        return graph;
    }
}


/**
 * Module exports
 */
module.exports = Robby;
