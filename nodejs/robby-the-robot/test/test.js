var expect = require('chai').expect;
var Robby = require('../robby.js');

describe('Testing class Robby', function () {
    it('should return function/class', function () {
        expect(Robby).to.be.a('function');
    });

    it('should create object', function () {
        expect(new Robby()).to.be.a("object")
    });

    it('should function with default options', function () {
        let robby = new Robby()
        let commands = robby.getCommands();

        expect(commands).to.be.a('array')
        expect(commands).to.eql(['f'])
    });

    it('Test field `S.......T` with power 10', function () {
        let robby = new Robby({ field: 'S.......T', power: 10 });
        expect(robby.getCommands()).to.eql(['rffrff'])
    });

    it('Test field `S.......T` with power 5', function () {
        let robby = new Robby({ field: 'S.......T', power: 5 });
        expect(robby.getCommands()).to.eql([])
    });

    it('Test field `S#.##...T` with power 20', function () {
        let robby = new Robby({ field: 'S#.##...T', power: 20 });
        expect(robby.getCommands()).to.eql([])
    });

    it('Test field `T.#...#...#.#####........##....S#...` with power 25', function () {
        let robby = new Robby({ field: 'T.#...#...#.#####........##....S#...', power: 25 });
        expect(robby.getCommands()).to.eql([])
    });

    it('Test field `S.......T` with power 6', function () {
        let robby = new Robby({ field: 'S.......T', power: 6 });
        expect(robby.getCommands()).to.eql(['rffrff'])
    });

    it('Test long field with power 400', function () {
        let robby = new Robby({ field: '................................................................###########.........#.........#.........#.#######.#.........#.#.......#.........#.#.#######.........#.#.#S.#............#.#.##.#............#.#....#............#.######............#...................###############........................................................................................................................T', power: 400 });
        expect(robby.getCommands()).to.eql(['rfrffrfffrffffrfffffflfflfffffffflfffffffflffffffffffffffrfffffff'])
    });

    it('Test very very long input', function () {
        let robby = new Robby({ field: 'S' + '.'.repeat(2998) + 'T', power: 10 });
        expect(robby.getCommands()).to.eql([])
    });

    
});




// test fixtures from description
// console.log(getCommands('T.S.', 10).join('') == 'f');
// console.log(getCommands('S.......T', 10).join('') == 'rffrff');
// console.log(getCommands('S.......T', 5).join('') == '');
// console.log(getCommands('S#.##...T', 20).join('') == '');
